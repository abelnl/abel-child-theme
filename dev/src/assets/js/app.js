
// init svg4everybody
svg4everybody();

//check if IE10
function getIEVersion() {
    var agent = navigator.userAgent;
    var reg = /MSIE\s?(\d+)(?:\.(\d+))?/i;
    var matches = agent.match(reg);
    if (matches != null) {
        return { major: matches[1], minor: matches[2] };
    }
    return { major: "-1", minor: "-1" };
}

var ie_version = getIEVersion();
var is_ie10 = ie_version.major == 10;

//console.log('is IE 10', is_ie10);

if (is_ie10) {
    $('body').addClass('IE10');
}

// init Scrollmagic controller
var controller = new ScrollMagic.Controller();

$(function(){
    
    // init foundation
    $(document).foundation();

    // fastclick 
    // FastClick.attach(document.body);

    // default slick 
    $('[data-init-slick]').slick();



    /* ANIME
    --------------------------------------------------------------------------------------------*/

    // default anime
    $('[data-anime-type="default"]').each(function(){

        var uuid = guid();
        $(this).attr('data-anime-id', uuid);

        new ScrollMagic.Scene({offset: 200, triggerElement: '[data-anime-id="'+ uuid  +'"]', triggerHook: 'onEnter',  reverse: false })
        .addTo(controller)
        .on("enter", function(e){
            anime({
                targets: ['[data-anime-id="'+ uuid +'"] [data-anime-elem]'],
                opacity: [0, 1],
                duration: 2000,
                delay: function(el, index) {
                    return index * 66;
                }
            });
        })

    });



    // transform-in
    $('[data-anime-type="transform-in"]').each(function(){

        var uuid = guid();
        $(this).attr('data-anime-id', uuid);

        new ScrollMagic.Scene({offset: 200, triggerElement: '[data-anime-id="'+ uuid  +'"]', triggerHook: 'onEnter', reverse: false })
        .addTo(controller)
        .on("enter", function(e){
            anime({
                targets: ['[data-anime-id="'+ uuid +'"] [data-anime-elem]'],
                translateY: ['20px', '0px'],
                opacity: [1],
                duration: 1200,
                elasticity: 0,
                delay: function(el, index) {
                    return index * 66;
                },
                complete: function() {
                    $('[data-anime-id="' + uuid + '"] [data-anime-elem]').css('transform', '');
                }
            });
        })

    });


    // show-scale
    $('[data-anime-type="scale"]').each(function(){

        var uuid = guid();
        $(this).attr('data-anime-id', uuid);

        new ScrollMagic.Scene({offset: 200, triggerElement: '[data-anime-id="'+ uuid  +'"]', triggerHook: 'onEnter',  reverse: false })
        .addTo(controller)
        .on("enter", function(e){
            anime({
                targets: ['[data-anime-id="'+ uuid +'"] [data-anime-elem]'],
                opacity: [.6, 1],
                scale: [1.03, 1.01],
                duration: 2000,
                elasticity: 100,
                delay: function(el, index) {
                    return index * 66;
                }
            });
        })

    });

    $('[data-anime-type="scale-slow"]').each(function(){

        var uuid = guid();
        $(this).attr('data-anime-id', uuid);

        new ScrollMagic.Scene({offset: 200, triggerElement: '[data-anime-id="'+ uuid  +'"]', triggerHook: 'onEnter',  reverse: false })
        .addTo(controller)
        .on("enter", function(e){
            anime({
                targets: ['[data-anime-id="'+ uuid +'"] [data-anime-elem]'],
                opacity: [.3, 1],
                scale: [1.1, 1.02],
                duration: 4000,
                elasticity: 0,
                easing: 'easeOutExpo',
                delay: function(el, index) {
                    return index * 80;
                }
            });
        })

    });


    // anime count
    $('[data-anime-type="count"]').each(function(){

        var uuid = guid();
        $(this).attr('data-anime-id', uuid);

        new ScrollMagic.Scene({offset: 0, triggerElement: '[data-anime-id="'+ uuid  +'"]', triggerHook: 'onEnter', reverse: false })
        .addTo(controller)
        .on("enter", function(e){
            var amount = {
                number: 0
            }
            var total = parseInt($('[data-anime-id="'+ uuid +'"]').data('anime-count'));
            
            anime({
                targets: amount,
                number: total,
                duration: 2000,
                round: true,
                easing: 'easeOutExpo',
                update: function() {
                    $('[data-anime-id="'+ uuid +'"] [data-anime-elem]').text(amount['number'])
                }
            });
        })

    });


    /* GO TO BY SCROLL
    --------------------------------------------------------------------------------------------*/
    $('[data-go-to-by-scroll]').click(function(e){
        e.preventDefault();

        var destination = $(this).attr('data-destination');
        var offset = $(this).attr('data-offset');
        var speed = $(this).attr('data-speed');
        var container = $(this).attr('data-container');

        goToByScroll(destination,offset,speed, container);
    });


    /* POP MOBILE MENU
    --------------------------------------------------------------------------------------------*/
    $('[data-toggle-mobile-menu]').on('click tap', function() {
        $('body').toggleClass('s_is-open_menu-mobile');
    });

    /* POP SEARCH INPUT
    --------------------------------------------------------------------------------------------*/
    $('[data-toggle-menu-search]').on('click tap', function(e) {

        e.preventDefault();
        
        $('body').toggleClass('s_is-open_menu-search');

        if($('body').hasClass('s_is-open_menu-search')) {
            $('[data-menu-search]').focus();
        } else {
            $('[data-menu-search]').val('');
        }
    });



    /* SCROLL TO CONTENT
    --------------------------------------------------------------------------------------------*/
    $('.ac_hero_link_to-content').on('click tap', function () {

        var scrollHeight = $('[data-s-type="hero"]').height();

        anime({
            targets: [document.querySelector('html'), document.querySelector('body')], // both to please Safari and Chrome
            scrollTop: parseInt(scrollHeight / 2),
            duration: 600,
            elasticity: 0,
            easing: 'easeOutExpo',
            delay: 0
        });

    });

    /* FULL SCREEN GALLERY
    --------------------------------------------------------------------------------------------*/
/*    $('.ac_image-gallery_full-screen_close').on('click tap', function() {
        $('body').removeClass('s_is-open_image-gallery_full-screen');
    });

    $('a.ac_image-gallery_item').on('click tap', function(e) {
        e.preventDefault();
        var openOnSLide = $(this).index();
        // console.log(openOnSLide); 
//        $('.ac_image-gallery_full-screen_container').slick('slickGoTo',(openOnSLide-1), true);
        $('body').addClass('s_is-open_image-gallery_full-screen');
    });
    */

    
    /* PRODUCTS FEED DEMO
    --------------------------------------------------------------------------------------------*/

    // var productFeed = [
    //     'http://cooking.chefduweb.nl/abelproducts/product1.php?json=true',
    //     'http://cooking.chefduweb.nl/abelproducts/product2.php?json=true',
    //     'http://cooking.chefduweb.nl/abelproducts/product3.php?json=true'
    // ]

    // $.each(productFeed, function( index, value ) {
        
    //     console.log(productFeed);

    //     $.ajax({url: productFeed[index], success: function(result){
    //         console.log(result);
    //     }});

    //     var content = '<a href="#" class="ac_item" data-s-amount-item data-anime-elem>' +
    //         '<div class="ac_item_container" data-border-bottom>' +
    //             '<div class="ac_item_content_label">' +
    //                 'Actie' +
    //             '</div>'+
    //             '<div class="ac_item_image-container">' +
    //                 '<figure class="ac_item_image a_contain" style="background-image: url()"></figure>'+
    //             '</div>'+
    //             '<div class="ac_item_content">' +
    //                 '<div class="ac_item_icon">' +
    //                     '<svg role="img"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="public/assets/symbols.svg#icon_ui_arrow-right"></use></svg>' +
    //                 '</div>' +
    //                 '<div class="ac_item_content_copy">' +
    //                     '<div class="ac_item_content_copy-above">' +
    //                         '<div class="ac_item_content_price">' +
    //                             '<div class="ac_item_content_price_before">van 999,95</div>' +
    //                             '<div class="ac_item_content_price_current">| voor 899,95</div>' +
    //                         '</div>' +
    //                     '</div>' +
    //                     '<div class="ac_item_content_title">' +
    //                         'title' +
    //                     '</div>' +
    //                      '<div class="ac_item_content_copy-below">' +
    //                         'description' +
    //                     '</div>' +
    //                 '</div>' +
    //             '</div>' +
    //         '</div>' +
    //     '</a>';

    //     $('#ac_products_feed').append(content);
    // });



}); // end jquery scope


/* GLOBAL FUNCTIONS
--------------------------------------------------------------------------------------------*/

function guid() {
function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
    .toString(16)
    .substring(1);
}
return s4() + s4() + s4() + s4();
}


function goToByScroll(destination, minusOffset, speed, container) {
    if(destination) {
        if(minusOffset === undefined) minusOffset = '0';
        if(speed === undefined) speed = '666';
        if(container === undefined) container = 'html, body';
        
        //console.log(destination, minusOffset, speed, container);

        var top = 0;
        top = ($("#" + destination).position().top - minusOffset).toFixed(0);

        $(container).animate({ scrollTop: top }, speed, "easeInOutQuad");
    }
}