Abel Child Theme
===========================

The default abel child theme, to be renamed and changed for each individual Abel Nederland client. 

---

## Requirements

| Prerequisite    | How to check | How to install
| --------------- | ------------ | ------------- |
| PHP >= 5.4.x    | `php -v`     | [php.net](http://php.net/manual/en/install.php) |
| PHP Shorttags	  | `php.ini` 	 | [enable shorttags](https://stackoverflow.com/questions/2185320/how-to-enable-php-short-tags) |
| Abel parent theme | - | [install the parent theme](https://bitbucket.org/abelnl/abel-website-platform) |

---

## Installation

Installation of this theme out-of-the-box is pretty easy: 

1. Clone it to a folder named after the client: `git clone git@bitbucket.org:abelnl/abel-child-theme.git {{clientname}}`
2. Place that folder in wp-content/themes
3. Change the style.css to include the client name
4. Enable the WordPress theme.

If you're using the [Abel deployment tools](https://bitbucket.org/abelnl/abel-deployment), you won't have to install anything manually. You only need tot replace {{Clientname}} in the theme's style.css with the actual client name.

---

## Contributing

Everyone is welcome to help [contribute](CONTRIBUTING.md) and improve this project. There are several ways you can contribute:

* Reporting issues
* Suggesting new features
* Writing or refactoring code
* Fixing [issues](https://github.com/cuisine-wp/cuisine/issues)



